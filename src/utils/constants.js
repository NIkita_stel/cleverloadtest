export const COLORS = {
    WHITE: '#FFF',
    IMAGE_LAYOUT: 'rgba(0, 0, 0, 0.5)',
    MAIN_COLOR: '#227ba0',
    BACKGROUND: '#f5f6f6',
    NOT_ACTIVE_FONT: '#d3d5d5',
    TITLE: '#8f9297',
};