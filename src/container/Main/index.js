import React, { PureComponent } from "react";
import {View, ScrollView, StatusBar, Image} from 'react-native';
// COMPONENTS
import TopCityImage from "../../components/TopCityImage/";
import Tiles from "../../components/Tiles";
// STYLES
import styles from "./styles";
import Activities from "../../components/Activities";
import Bottom from "../../components/Bottom";

export default class Main extends PureComponent {
  render() {
    return (
            <View style = { styles.container }>
                <StatusBar barStyle="light-content" />
                    <TopCityImage />
            </View>
    );
  }
}
