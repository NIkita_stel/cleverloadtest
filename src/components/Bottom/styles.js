import { StyleSheet, Dimensions } from 'react-native';
import { COLORS } from '../../utils/constants';
const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
    bottomContainer:{
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        height: 70,
        width: width,
        flexDirection: "row",
        justifyContent: 'space-around',
        backgroundColor: COLORS.WHITE,
        alignItems: 'center',
    },
    botImgCont:{
        flexDirection: 'column',
        alignContent: 'center',
        alignItems: 'center',
    },
    plusIcon:{
      width: 80,
      height: 80,
        top: -16,
    },
    bottomTxt: {
       fontSize: 9,
        marginTop: 5,
        color: COLORS.NOT_ACTIVE_FONT,
    },
    profileImg: {
        height: 28,
        width: 28,
    },
    active:{
        color: COLORS.MAIN_COLOR
    },
    botImg: {
        height: 26.5,
    }
});

export default styles;