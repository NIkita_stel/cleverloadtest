// CORE
import React, { PureComponent } from "react";
import { View, Text, Image } from "react-native";
// STYLES
import styles from "./styles";

export default class Bottom extends PureComponent {
  render() {
    return (
      <View style={styles.bottomContainer}>
        <View style={styles.botImgCont}>
          <Image
            style={styles.botImg}
            source={require("../../assets/icon/cityIcnActive.png")}
          />
          <Text style={[styles.bottomTxt, styles.active]}> My City</Text>
        </View>
        <View style={styles.botImgCont}>
          <Image source={require("../../assets/icon/likesIcn.png")} />
          <Text style={styles.bottomTxt}> Favourite</Text>
        </View>
        <View style={styles.botImgCont}>
          <Image
            style={styles.plusIcon}
            source={require("../../assets/icon/plus.png")}
          />
        </View>
        <View style={styles.botImgCont}>
          <Image source={require("../../assets/icon/notifsIcn.png")} />
          <Text style={styles.bottomTxt}>Notifications</Text>
        </View>
        <View style={styles.botImgCont}>
          <Image
            style={styles.profileImg}
            source={require("../../assets/icon/profileIcn.jpg")}
          />
          <Text style={styles.bottomTxt}>Profile</Text>
        </View>
      </View>
    );
  }
}
