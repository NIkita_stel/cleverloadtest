import React from "react";
import { Text, TouchableOpacity } from "react-native";
import styles from './styles';

const Button = (props) => {
    const { text, onPressBtn } = props;
    return (
        <TouchableOpacity onPress = { onPressBtn }
                          style = { styles.button }>
            <Text style = { styles.btnText }>{ text }</Text>
        </TouchableOpacity>
    )
};

export default Button;
