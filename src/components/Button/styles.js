import { StyleSheet } from 'react-native';
import {COLORS} from "../../utils/constants";

const styles = StyleSheet.create({
    button: {
        width: 70,
        borderRadius: 15,
        backgroundColor: COLORS.MAIN_COLOR,
        height: 25,
        justifyContent: 'center',
        alignItems: 'center',
        top: -5,
        right: 20,
    },
    btnText: {
        color: COLORS.WHITE,
        fontSize: 13,
        fontWeight: '600'
    }
});

export default styles;