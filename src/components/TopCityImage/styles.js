import { StyleSheet, Dimensions } from 'react-native';
import { COLORS } from '../../utils/constants';
const { width, height } = Dimensions.get('window');


const styles = StyleSheet.create({
    containerImage: {
        position: 'relative',
        top: 0,
        right: 0,
        left: 0,
    },
    cityIconCont: {
        position: 'absolute',
        zIndex: 3,
        width: width,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    topImageIcon: {
        flexDirection: 'row',
        zIndex: 3,
        top: 68,
        right: 20,
    },
    mapIcon: {
      marginRight: 20,
    },
    topImage: {
        // resizeMode: 'cover',
        width: width,
        // height: 250,
        top: -45,
        left: 0,
        right: 0,
    },
    btnImage: {
        position: 'absolute',
        top: 70,
        left: 25,
        zIndex: 99999,
        fontSize: 19,
        borderRadius: 50,
        borderWidth: 1,
        borderColor: COLORS.WHITE,
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 3,
        paddingBottom: 3,
    },
    btnImageText: {
        color: COLORS.WHITE,
    },
    cityName: {
        top: 120,
        zIndex: 99999,
        left: 25,
        color: COLORS.WHITE,
        fontSize: 30,
        fontWeight: "700",
    },
    animatedTop: {
        position: 'absolute',
        bottom: 150,
        backgroundColor: 'white',
        width: '100%',
        left: 0,
        right: 0,
        flexDirection: 'row',
        justifyContent: "space-between",
    },
    animtop: {
        position: 'relative',
    },
    cityMargin: {
        marginRight: 20,
    }
});

export default styles;