// CORE
import React, { PureComponent } from "react";
import {
  View,
  Image,
  TouchableOpacity,
  Text,
  Animated,
  ScrollView
} from "react-native";
// COMPONENTS
import Tiles from "../Tiles";
import Activities from "../Activities";
import People from "../People";
import Bottom from "../Bottom";
// STYLE
import styles from "./styles";

export default class TopCityImage extends PureComponent {
  state = {
    scrollY: new Animated.Value(0)
  };

  changeCity = () => {};

  render() {

    const topImageHeight = this.state.scrollY.interpolate({
      inputRange:  [0, 200],
      outputRange: [200, 0],
      extrapolate: "clamp"
    });


    return (
      <View style={{ height: "100%" }}>
        <View style={styles.containerImage}>
          <View style={styles.cityIconCont}>
            <View>
              <TouchableOpacity
                style={styles.btnImage}
                onPress={this.changeCity}
              >
                <Text style={styles.btnImageText}>Change City</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.topImageIcon}>
              <Image
                style={styles.mapIcon}
                source={require("../../assets/icon/mapIcnWhite.png")}
              />
              <Image source={require("../../assets/icon/searchIcnWhite.png")} />
            </View>
          </View>
          <Text style={styles.cityName}>Dubai</Text>
          <Animated.View  style={{
              height: topImageHeight,}}>
            <Animated.Image
              style={styles.topImage}
              height = {topImageHeight}
              // style={{ position: 'relative',
              //     top: 0,
              //     right: 0,
              //     left: 0,
              //     height: topImageHeight,
              //     resizeMode: 'cover'}}
              source={require(`../../assets/images/dubai_city.jpeg`)}
            />
          </Animated.View>
        </View>
        <ScrollView onScroll = { Animated.event([{
          nativeEvent: { contentOffset : { y: this.state.scrollY }
          }}])
        }
        scrollEventThrottle = {16}
                    contentContainerStyle = {{ paddingBottom: 70}}
        >
        {/*<ScrollView>*/}
          <Tiles />
          <Activities />
          <People />
        </ScrollView>
        <Bottom />
      </View>
    );
  }
}
