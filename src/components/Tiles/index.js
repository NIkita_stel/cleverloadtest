// CORE
import React, { PureComponent } from "react";
import { View, Text } from "react-native";
// COMPONENTS
import Tile from "./Tile";
import Button from "../Button";
// STYLE
import styles from "./styles";

export default class Tiles extends PureComponent {
  chooseHotel = () => {
    console.log("Hotels");
  };

  render() {
    return (
      <View style={styles.tilesContainer}>
        <View style={styles.infoPanel}>
          <Text style={styles.panelTxt}>What's in Dubai?</Text>
          <Button text="Hotels" onPressBtn={this.chooseHotel} />
        </View>
        <Tile />
      </View>
    );
  }
}
