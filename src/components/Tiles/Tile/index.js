// CORE
import React, { PureComponent } from "react";
import { View, Text, Image } from "react-native";
// STYLE
import styles from "./styles";
// ICONS
import MaterialCommunityIcon from "react-native-vector-icons/MaterialCommunityIcons";
import Entypo from "react-native-vector-icons/Entypo";
import MaterialIcon from "react-native-vector-icons/MaterialIcons";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";

export default class Tile extends PureComponent {
  render() {
    return (
      <View style={styles.tileContainer}>
        <View style={styles.tileBlock1}>
          <View style={styles.tile}>
            <View style={styles.tileLayout} />
            <Image
              style={styles.tileImage}
              source={require("../../../assets/images/entertainment.jpg")}
            />
            <View style={styles.txtCont}>
              <MaterialCommunityIcon name="boombox" color="white" size={28} />
              <Text style={styles.tileText}> Entertainment </Text>
            </View>
          </View>
          <View style={[styles.tile, styles.marginTile]}>
            <View style={styles.tileLayout} />
            <Image
              style={styles.tileImage}
              source={require("../../../assets/images/events.jpg")}
            />
            <View style={styles.txtCont}>
              <Entypo name="ticket" color="white" size={28} />
              <Text style={styles.tileText}> Events </Text>
            </View>
          </View>
          <View style={styles.tile}>
            <View style={styles.tileLayout} />
            <Image
              style={styles.tileImage}
              source={require("../../../assets/images/restaurants.jpg")}
            />
            <View style={styles.txtCont}>
              <MaterialIcon size={28} name="restaurant" color="white" />
              <Text style={styles.tileText}> Restaurants </Text>
            </View>
          </View>
        </View>
        <View style={styles.tileBlock2}>
          <View style={styles.tile}>
            <View style={styles.tileLayout} />
            <Image
              style={styles.tileImage}
              source={require("../../../assets/images/breakfast.jpg")}
            />
            <View style={styles.txtCont}>
              <MaterialCommunityIcon name="hamburger" size={28} color="white" />
              <Text style={styles.tileText}> Breakfast </Text>
            </View>
          </View>
          <View style={[styles.tile, styles.marginTile]}>
            <View style={styles.tileLayout} />
            <Image
              style={styles.tileImage}
              source={require("../../../assets/images/masajid.jpg")}
            />
            <View style={styles.txtCont}>
              <FontAwesome5 size={25} color="white" name="mosque" />
              <Text style={styles.tileText}> Masajid </Text>
            </View>
          </View>
          <View style={styles.tile}>
            <View style={styles.tileLayout} />
            <Image
              style={styles.tileImage}
              source={require("../../../assets/images/coffee_cup.jpg")}
            />
            <View style={styles.txtCont}>
              <MaterialCommunityIcon name="coffee" color="white" size={28} />

              <Text style={styles.tileText}> Coffee Cup </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
