import { StyleSheet } from 'react-native';
import {COLORS} from "../../../utils/constants";

const styles = StyleSheet.create({
    tileContainer:{
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center',
        marginBottom: 35,
    },
    tileBlock1: {
      flexDirection: 'row',
        marginBottom: 10,
    },
    marginTile: {
        marginRight: 10,
        marginLeft: 10,
    },
    tileBlock2: {
        flexDirection: 'row',
    },
    tile: {
      width : 105,
      height: 75,
    },
    tileIcons: {
        // width: 40,
        // height: 50,
        // bottom: 100,
        // right: 0,
        zIndex: 4,
        width: 50,
        height: 50,
        // position: "absolute",
        // bottom: 50,
        // zIndex:999999999,

    },
    tileLayout:{
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: COLORS.IMAGE_LAYOUT,
        height: '100%',
        width: '100%',
        borderRadius: 10,
        zIndex: 2,
    },
    tileImage: {
        width : '100%',
        height: '100%',
        resizeMode:'cover',
        borderRadius: 10,
        zIndex: 1,
        // position: 'relative',
    },
    tileText: {
        color: COLORS.WHITE,
        fontSize: 12,
        // textAlign: 'center',
        // top: 35,
        // left: 10,
    },
    txtCont: {
        position: 'absolute',
        flexDirection: 'column',
        zIndex: 3,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',
    }
});

export default styles;