import { StyleSheet, Dimensions } from 'react-native';
import {COLORS} from "../../utils/constants";
const {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
    tilesContainer: {
       top:6,
        // zIndex: 2,
    },
    infoPanel: {
        width: width,
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
    },
    panelTxt:{
        paddingLeft: 20,
        paddingBottom: 10,
        fontSize: 16,
        color: COLORS.TITLE,
    },
});

export default styles;