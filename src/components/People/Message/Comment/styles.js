import { StyleSheet } from "react-native";
import { COLORS } from '../../../../utils/constants';

const styles = StyleSheet.create({
  commentContainer: {
    flexDirection: "row",
    alignItems: "flex-start"
  },
  commentTxtBlock: {
    backgroundColor: COLORS.WHITE,
    width: 310,
    borderRadius: 15,
    padding: 20
  },
  commentTxt: {
    fontSize: 11
  }
});

export default styles;
