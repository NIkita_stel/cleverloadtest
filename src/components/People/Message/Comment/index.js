// CORE
import React, { PureComponent } from "react";
import { View, Text } from "react-native";
import { Avatar } from "react-native-elements";
// STYLES
import styles from "./styles";

export default class Comment extends PureComponent {
  render() {
    return (
      <View style={styles.commentContainer}>
        <View>
          <Avatar
            rounded
            source={{
              uri:
                "https://firebasestorage.googleapis.com/v0/b/imagesdb-7ec73.appspot.com/o/comment.jpg?alt=media&token=3123531f-3e3d-420b-8713-b7b2107d954b"
            }}
            onPress={() => console.log("Works!")}
            activeOpacity={0.7}
            containerStyle={{ height: 25, width: 25, marginRight: 10 }}
            avatarStyle={{ height: 25, width: 25 }}
          />
        </View>
        <View style={styles.commentTxtBlock}>
          <Text style={styles.commentTxt}>
            Yeeeees come now .. it is so interesting !!! it will end soon
          </Text>
        </View>
      </View>
    );
  }
}
