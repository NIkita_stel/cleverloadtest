// CORE
import React, { PureComponent } from "react";
import { View, Text, Image } from "react-native";
import { Avatar } from "react-native-elements";
// STYLE
import styles from "./styles";
// COMPONENTS
import Comment from "./Comment";

export default class Message extends PureComponent {
  render() {
    return (
      <View style={styles.msgScroll}>
        <View style={styles.msgContainer}>
          <View style={styles.avatarBlock}>
            <Avatar
              size="small"
              rounded
              source={{
                uri:
                  "https://firebasestorage.googleapis.com/v0/b/imagesdb-7ec73.appspot.com/o/osama.jpg?alt=media&token=d44fade5-c0a8-4727-9fae-0f01d0b65956"
              }}
              onPress={() => console.log("Works!")}
              activeOpacity={0.7}
              containerStyle={{ marginRight: 10 }}
            />
            <Text style={styles.hours}> 5h </Text>
          </View>

          <View style={styles.mainMsgBlock}>
            <View style={styles.msgTop}>
              <View>
                <Text style={styles.blockTxt}> Osama Alnajjar </Text>
              </View>

              <View style={styles.macDonald}>
                <Image source={require("../../../assets/icon/oval5.png")} />
                <Text style={styles.blockTxt}> Mccdonalds </Text>
              </View>
            </View>

            <View styles={styles.msgText}>
              <Text style={{ marginVertical: 5, fontSize: 13 }}>
                {" "}
                Very interesting things art happening now in Alothaim Mall ..
                Come Now !!!!
              </Text>
            </View>
            <View style={styles.likeCmntTxtCont}>
              <Image
                style={styles.txtMargin}
                source={require("../../../assets/icon/likeIcn.png")}
              />
              <Text style={[styles.countNumbers, styles.txtMargin]}>234</Text>
              <Image
                style={styles.txtMargin}
                source={require("../../../assets/icon/line15.png")}
              />
              <Text style={[styles.countNumbers, styles.txtMargin]}>11</Text>
            </View>
              <Image style={{ position: 'absolute', right: 0, bottom: 2}} source = {require('../../../assets/icon/stroke1.png')} />

          </View>
        </View>
        <View style={styles.msgContainer}>
          <View style={styles.avatarBlock}>
            <Avatar
              size="small"
              rounded
              source={{
                uri:
                  "https://firebasestorage.googleapis.com/v0/b/imagesdb-7ec73.appspot.com/o/omar.jpg?alt=media&token=2a9f6e14-ffad-455e-bd0a-aa880ac9640a"
              }}
              onPress={() => console.log("Works!")}
              activeOpacity={0.7}
              containerStyle={{ marginRight: 10 }}
            />
            <Text style={styles.hours}> 2m </Text>
          </View>

          <View style={styles.mainMsgBlock}>
            <View style={styles.msgTop}>
              <View>
                <Text style={styles.blockTxt}> Omar Hammad </Text>
              </View>

              <View style={styles.macDonald}>
                <Image source={require("../../../assets/icon/oval5.png")} />
                <Text style={styles.blockTxt}> Mccdonalds </Text>
              </View>
            </View>

            <View styles={styles.msgText}>
              <Text style={{ marginVertical: 5, fontSize: 13 }}>
                {" "}
                Very interesting things art happening now in Alothaim Mall ..
                Come Now !!!!
              </Text>
              <Image
                style={{ width: 250, height: 120, borderRadius: 15 }}
                source={require("../../../assets/images/entertainment.jpg")}
              />
            </View>
            <View style={styles.likeCmntTxtCont}>
              <Text style={[styles.countNumbers, styles.txtMargin]}>11</Text>
              <Image
                style={styles.txtMargin}
                source={require("../../../assets/icon/line15.png")}
              />
              <Text style={[styles.countNumbers, styles.txtMargin]}>234</Text>
              <Image
                style={styles.txtMargin}
                source={require("../../../assets/icon/likeIcn.png")}
              />
            </View>
            <Image style={{ position: 'absolute', right: 0, bottom: 2}} source = {require('../../../assets/icon/stroke1.png')} />
          </View>
        </View>
        <Comment />
      </View>
    );
  }
}
