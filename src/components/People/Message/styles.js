import { StyleSheet, Dimensions } from 'react-native';
import { COLORS } from '../../../utils/constants';
const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
    msgScroll:{
        flexDirection: 'column',
    },
    msgContainer: {
      width: width - 30,
      backgroundColor: COLORS.WHITE,
        // marginLeft: 25,
        // marginRight: 25,
        borderRadius: 15,
        flexDirection: 'row',
        padding: 25,
        justifyContent: 'center',
        marginBottom: 20,
    },
    mainMsgBlock: {
        flexDirection: 'column',
    },
    macDonald:{
        flexDirection: 'row',
        alignItems: 'center',
    },
    msgTop: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    avatarBlock:{
        flexDirection: 'column',
        alignItems: 'center',
    },
    hours: {
        color: COLORS.NOT_ACTIVE_FONT,
        fontSize: 10,
        marginRight: 10,
        marginTop: 5,
    },
    blockTxt: {
        fontSize: 10,
        color: COLORS.MAIN_COLOR,
    },
    countNumbers: {
        color: COLORS.MAIN_COLOR,
    },
    txtMargin:{
        marginRight: 10,
    },
    likeCmntTxtCont: {
      flexDirection: 'row',
        alignItems: 'center',
        marginTop: 15,
    },
    msgText: {
    }
});

export default styles;