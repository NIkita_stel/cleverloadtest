import React, { PureComponent } from "react";
import { View, Text, Image } from "react-native";
import Message from "./Message";
import styles from "./styles";

export default class People extends PureComponent {
  render() {
    return (
      <View>
        <Text style={styles.pplTextTop}>People in Your City</Text>
        <View style={styles.pplContainer}>
          <Message />
        </View>
      </View>
    );
  }
}
