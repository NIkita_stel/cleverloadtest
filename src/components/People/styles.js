import { StyleSheet } from 'react-native';
import {COLORS} from "../../utils/constants";

const styles = StyleSheet.create({
    pplContainer: {
        backgroundColor: COLORS.BACKGROUND,
        alignItems: 'center',
    },
    pplTextTop: {
        fontSize: 16,
        color: COLORS.TITLE,
        backgroundColor: COLORS.BACKGROUND,
        paddingBottom: 20,
        paddingLeft: 20,
    },
});

export default styles;