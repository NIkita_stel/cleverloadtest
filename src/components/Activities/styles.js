import { StyleSheet, Dimensions } from 'react-native';
import {COLORS} from "../../utils/constants";
const {width} = Dimensions.get('window');

const styles = StyleSheet.create({
    activityContainer: {
        // flexDirection: 'row',
        // height: 200,
        // flexWrap: 'wrap',
        // justifyContent: 'center',
        // marginLeft: 20,
        // marginRight: 20,
        backgroundColor: COLORS.BACKGROUND,
    },
    infoPanel: {
        // width: width,
        // justifyContent: 'space-between',
        // flexDirection: 'row',
        // alignItems: 'flex-star',
        paddingLeft: 20,
        paddingTop: 10,
    },
    infoPanelText: {
        fontSize: 16,
        color: COLORS.TITLE,
    },
});

export default styles;