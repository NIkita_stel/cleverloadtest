// CORE
import React, { PureComponent } from "react";
import { View, Text, ScrollView } from "react-native";
// COMPONENTS
import Activity from "./Activity";
// INSTRUMENTS
import fakeDB from "../../fakeDB/fakeDB";
// STYLES
import styles from "./styles";

export default class Activities extends PureComponent {
  render() {
    const activity = fakeDB.activities.map( (a) => (
      <Activity
        key           = { a.id }
        activityName  = { a.activity }
        activityImage = { a.image }
        tickets       = { a.ticketsAvail }
      />
    ));

    return (
      <View style={styles.activityContainer}>
        <View style= { styles.infoPanel}>
          <Text style = { styles.infoPanelText }>Active now in your city</Text>
        </View>
        <ScrollView horizontal = { true }
                    showsHorizontalScrollIndicator = { false }>
            {activity}
        </ScrollView>
      </View>
    );
  }
}
