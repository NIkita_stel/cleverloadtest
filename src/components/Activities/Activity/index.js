import React, { PureComponent } from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import styles from "./styles";
import Button from "../../Button";

export default class Activity extends PureComponent {
  render() {
    const { activityName, activityImage, tickets } = this.props;

    return (
      <View style={styles.activity}>
        <View style={styles.activityLayout} />
          <TouchableOpacity style = { styles.activityBtn }>
              <Text style = { styles.activityBtnTxt }> Get it Now </Text>
          </TouchableOpacity>
        <Image style={styles.activityImage} source={{ uri: activityImage }} />
        <View style = { styles.activityTextCont }>
          <Text style={styles.activityTextParty}>{activityName}</Text>
          <Text style={styles.activityTextTicket}>
            {`${tickets} Tickets Remaining`}
          </Text>
        </View>
      </View>
    );
  }
}
