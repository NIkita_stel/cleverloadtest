import { StyleSheet } from 'react-native';
import { COLORS } from '../../../utils/constants';

const styles = StyleSheet.create({
    activity:{
        // flex: 1,
        width: 180,
        height: 180,
        margin: 20,
        position: 'relative',
    },
    activityImage: {
        width: 180,
        height: 180,
        borderRadius: 10,
    },
    activityTextParty: {
        color: COLORS.WHITE,
        fontSize: 24,
        left: 15,
        fontWeight: "700"
    },
    activityTextTicket:{
        color: COLORS.WHITE,
        fontSize: 15 ,
        left: 15,
    },
    activityTextCont: {
        position: 'absolute',
        bottom: 20,
        width: 160,
        zIndex: 3,
    },
    activityLayout:{
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: COLORS.IMAGE_LAYOUT,
        height: '100%',
        width: '100%',
        borderRadius: 10,
        zIndex: 2
    },
    activityBtn:{
        width: 100,
        backgroundColor: COLORS.WHITE,
        zIndex: 2,
        borderRadius: 15,
        padding: 8,
        position: 'absolute',
        right: 15,
        top: 12,
    },
    activityBtnTxt: {
        fontSize: 15,
        fontWeight: '700',
        color: COLORS.MAIN_COLOR,
    }
});

export default styles;